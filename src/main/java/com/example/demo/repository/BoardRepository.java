package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.Board;

public interface BoardRepository extends JpaRepository<Board, Integer>{
	
	@Modifying
	@Query(value = "update Board b set b.hits=b.hits+1 where b.bno=:bno")
	void updateHits(@Param("bno")int no); // bno 

}
