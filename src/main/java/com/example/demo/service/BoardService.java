package com.example.demo.service;

import org.springframework.data.domain.Page;

import com.example.demo.dto.BoardDTO;
import com.example.demo.entity.Board;
import com.example.demo.entity.Member;

public interface BoardService {

	int write(BoardDTO dto); // 글쓰기
	Page<BoardDTO> getList(int pageNum); //게시물 목록
	BoardDTO read(int bno); // 조회
	void modify(BoardDTO dto); // 게시물 수정
	void delete(int bno); //게시물 삭제
	void updateHits(int bno); // 조회수
	//Page<BoardDTO> search(String keyword, int page); // 게시물 검색
	
	default Board dtoToEntity(BoardDTO dto) {
		Member member = Member.builder().id(dto.getWriter()).build();
		Board entity = Board.builder()
				.bno(dto.getBno())
				.title(dto.getTitle())
				.content(dto.getContent())
				.writer(member)
				.hits(dto.getHits())
				.build();
		return entity;
	}

	default BoardDTO entityToDTO(Board entity) {
		BoardDTO dto = BoardDTO.builder()
				.bno(entity.getBno())
				.title(entity.getTitle())
				.content(entity.getContent())
				.writer(entity.getWriter().getId())
				.hits(entity.getHits())
				.regdate(entity.getRegdate())
				.moddate(entity.getModdate())
				.build();
		return dto;
		}
}

