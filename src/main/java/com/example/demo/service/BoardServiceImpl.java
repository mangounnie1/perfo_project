package com.example.demo.service;



import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.dto.BoardDTO;
import com.example.demo.entity.Board;
import com.example.demo.repository.BoardRepository;

import jakarta.transaction.Transactional;

@Service
public class BoardServiceImpl implements BoardService {

	@Autowired
	BoardRepository boardRepository;
	
	@Override
	public int write(BoardDTO dto) {
		System.out.println(dto);
		Board entity = dtoToEntity(dto);
		boardRepository.save(entity);
		return entity.getBno();
	}

	@Override
	public Page<BoardDTO> getList(int pageNum) {
		int page = (pageNum == 0) ? 0 : pageNum -1;
		Pageable pageable = PageRequest.of(page, 5, Sort.by("bno").descending());
		Page<Board> boardpage = boardRepository.findAll(pageable);
		Page<BoardDTO> dtopage = boardpage.map(entity -> entityToDTO(entity));
		return dtopage;
	}

	@Override
	public BoardDTO read(int bno) {
		Optional<Board> result = boardRepository.findById(bno);
		if(result.isPresent()) {
			Board entity = result.get();
			return entityToDTO(entity);
		} else {
			return null;
		}
	}

	@Override
	public void modify(BoardDTO dto) {
		Optional<Board> result = boardRepository.findById(dto.getBno());
		if(result.isPresent()) {
			Board entity = result.get();
			entity.setTitle(dto.getTitle());
			entity.setContent(dto.getContent());
			boardRepository.save(entity);
		}
	}

	@Override
	public void delete(int bno) {
		Board entity = boardRepository.findById(bno).orElseThrow(() -> new RuntimeException("게시물을 찾을 수 없습니다."));
		boardRepository.delete(entity);
	}

	@Transactional
	public void updateHits(int bno) {
		boardRepository.updateHits(bno);	
	}
	
//	@Override
	//public Page<BoardDTO> search(String keyword, int page){
		
	//}


}
