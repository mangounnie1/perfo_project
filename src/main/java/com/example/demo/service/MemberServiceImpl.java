package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.example.demo.dto.MemberDTO;
import com.example.demo.entity.Member;
import com.example.demo.repository.BoardRepository;
import com.example.demo.repository.MemberRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Service
public class MemberServiceImpl implements MemberService {

	
	@Autowired
	MemberRepository memberRepository;
	
	@Override
	public Page<MemberDTO> getList(int pageNum) {  // 가입일 기준 5명씩, 목록화
		int page = (pageNum == 0) ? 0 : pageNum - 1;
		Pageable pageable = PageRequest.of(page, 5, Sort.by("regdate").descending());
		Page<Member> entityPage = memberRepository.findAll(pageable);
		Page<MemberDTO> dtoPage = entityPage.map(entity -> entityToDTO(entity));
		return dtoPage;
	}

	@Override
	public boolean signUp(MemberDTO dto) {
		String id = dto.getId(); // 아이디 중복체크
		MemberDTO getDto = read(id); // 상세조회
		if(getDto != null) {
			System.out.println("사용 중인 아이디입니다.");
			return false;
		}
		Member entity = DtoToEntity(dto); // 패스워드 암호화
		PasswordEncoder passwordEncoder =  new BCryptPasswordEncoder(); // 패스워드 인코더로 암호화
		String hashpassword = passwordEncoder.encode(entity.getPassword());
		entity.setPassword(hashpassword);
		memberRepository.save(entity); //entity에 저장하는 건 데이터로 저장함
		return true;
	}

	@Override //회원 조회
	public MemberDTO read(String id) {
		Optional<Member> result = memberRepository.findById(id);
		if(result.isPresent()) {
			Member member = result.get();
			return entityToDTO(member);
		} else {
			return null;
		}
	}

	@Override
	public void modify(MemberDTO dto) {
		Optional<Member> result = memberRepository.findById(dto.getId());
		if(result.isPresent()) {
			Member entity = result.get();
			entity.setId(dto.getId());
			entity.setPassword(dto.getPassword());
			entity.setName(dto.getName());
			entity.setRole(dto.getRole());
			memberRepository.save(entity);
		}
		
	}

	@Override
	public void delete(String id) {
		//Member member = memberRepository.findById(id).orElseThrow(()-> new IllegalArgumentException("Invalid member ID : " + id));
		// 해당 회원이 작성한 게시물 삭제
		//List<Board> boards = BoardRepository.findAllByWriter(member);
		//boardRepository.deleteAll(boards);
		memberRepository.deleteById(id);
	}

}
