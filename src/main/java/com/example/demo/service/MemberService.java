package com.example.demo.service;

import org.springframework.data.domain.Page;

import com.example.demo.dto.MemberDTO;
import com.example.demo.entity.Member;

public interface MemberService {
	Page<MemberDTO> getList(int pageNum); // 회원목록
	boolean signUp(MemberDTO dto); //가입
	MemberDTO read(String id); //조회
	void modify(MemberDTO dto); //수정
	void delete(String id); //삭제
	
	default MemberDTO entityToDTO(Member entity) {
		MemberDTO dto = MemberDTO.builder()
				.id(entity.getId())
				.password(entity.getPassword())
				.name(entity.getName())
				.role(entity.getRole())
				.regdate(entity.getRegdate())
				.moddate(entity.getModdate())
				.build();
		return dto;
				
	}
	
	default Member DtoToEntity(MemberDTO dto) {
		Member entity = Member.builder()
				.id(dto.getId())
				.password(dto.getPassword())
				.name(dto.getName())
				.role(dto.getRole())
				.build();
		return entity;
	}
}
