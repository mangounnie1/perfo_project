package com.example.demo.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.dto.BoardDTO;
import com.example.demo.service.BoardService;

@Controller
@RequestMapping("/board")
public class BoardController {
	@Autowired
	BoardService service;
	
	@GetMapping("/list")
	public void list(@RequestParam(defaultValue = "0")int page, Model model) {
		Page<BoardDTO> list = service.getList(page);
		model.addAttribute("list", list);
	}
	
	@GetMapping("/write")
	public void write() {
	}
	@PostMapping("/write")
	public String writePost(BoardDTO dto, RedirectAttributes ra, Principal principal) {
		String id = principal.getName();
		dto.setWriter(id);
		int bno = service.write(dto);
		ra.addFlashAttribute("msg", bno);
		return "redirect:/board/list";
	}
	
	@GetMapping("/read")
	public void read(int bno, @RequestParam(defaultValue = "0")int page, Model model) {
		service.updateHits(bno);
		BoardDTO dto = service.read(bno);
		model.addAttribute("dto", dto);
		model.addAttribute("page", page);
	}

	@GetMapping("/modify")
	public String modify(int bno, Model model, Principal principal, RedirectAttributes ra) {
		BoardDTO dto = service.read(bno);
		String writer = dto.getWriter();
		String loginId = principal.getName();
		if(!writer.equals(loginId)) { // 작성자와 로그인한 아이디 비교, 사용자가 다른경우 수정을 허용하지 않고 글 목록으로 이동
			ra.addAttribute("msg","본인이 작성한 글이~");
			return"redirect:/board/list";
		}
		model.addAttribute("dto", dto);
		return "board/modify";
	}
	@PostMapping("/modify")
	public String modifyPost(BoardDTO dto, RedirectAttributes ra, Principal principal) {
		String id = principal.getName();
		dto.setWriter(id);
		BoardDTO boardDTO = service.read(dto.getBno());
		if(!boardDTO.getWriter().equals(id)) {
			ra.addAttribute("msg", "본인이 작성한 글이 아니므로 수정할 수 없습니다.");
			return "redirect:/board/list";
		}
		service.modify(dto);
		ra.addAttribute("bno", dto.getBno());
		return "redirect:/board/read";
	}
	
	@PostMapping("/delete")
	public String delete(int bno) {
		service.delete(bno);
		return"redirect:/board/list";
	}
}
