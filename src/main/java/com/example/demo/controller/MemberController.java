package com.example.demo.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.dto.MemberDTO;
import com.example.demo.service.MemberService;

@Controller
@RequestMapping("/member")
public class MemberController {
	
	@Autowired
	MemberService service;

	@GetMapping("/list")
	public void list(@RequestParam(defaultValue = "0")int page, Model model) {
		Page<MemberDTO> list = service.getList(page);
		model.addAttribute("list", list);
	}
	
	@GetMapping("/signUp")
	public String signUp() {
		return"member/signUp";
	}
	@PostMapping("/signUp")
	public String signUpMember(MemberDTO dto, RedirectAttributes ra) {
		boolean isSuccess = service.signUp(dto);
		if(isSuccess) {
			return "redirect:/home"; // 등록성공시 홈 or 로그인화면?
		} else {
			ra.addFlashAttribute("msg", "중복");
			return "redirect:/member/signUp"; // 등록 실패시 회원가입으로 돌아가기
		}
	}
	
	@GetMapping("/read")
	public void read(String id, @RequestParam(defaultValue = "0")int page, Model model) {
		MemberDTO dto = service.read(id);
		model.addAttribute("dto", dto);
		model.addAttribute("page", page);
	}
	
	@GetMapping("/modify")
	public void modify(String id, Model model) {
		MemberDTO dto = service.read(id);
		model.addAttribute("dto", dto);
	}
	@PostMapping("/modify")
	public String modifyMember(MemberDTO dto, RedirectAttributes ra) {
		service.modify(dto);
		ra.addAttribute("id",dto.getId());
		return "redirect:/member/read";
	}
	
	@PostMapping("/delete")
	public String deleteMember(String id) {
		service.delete(id);
		return "redirect:/logout";
	}
	
	@GetMapping("/idcheck")
	public @ResponseBody HashMap<String, Boolean> idCheck(String id){
		HashMap<String, Boolean> result = new HashMap<String, Boolean>();
		MemberDTO dto = service.read(id);
		if(dto != null) {
			result.put("isDuplicate", true);			
		} else { 
			result.put("isDuplicate", false);
		}
		return result;
	}
}



