package com.example.demo.entity;

import org.hibernate.annotations.ManyToAny;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@Entity
public class Board extends BaseEntity{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int bno; //게시물 번호
	
	@Column(length = 300, nullable = false)
	private String title; //제목
	
	@Column(length = 3000, nullable = false)
	private String content; // 내용
	
	@Column
	private int hits; // 조회수
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = false)
	private Member writer; //작성자

	
	
	/*
	 * JPA는 연관된 엔티티를 함께 로딩합니다. 이때, fetchType.LAZY 옵션을 사용하면 해당 연관 엔티티를 지연 로딩합니다. 
	 * 즉, 해당 연관 엔티티가 실제로 사용되기 전까지는 데이터베이스에서 로딩되지 않으며, 이를 통해 불필요한 데이터 로딩을 방지하고 
	 * 성능을 향상시킬 수 있습니다.
	예를 들어, A 엔티티가 B 엔티티와 연관되어 있다면, fetchType.LAZY 옵션을 사용하면 A 엔티티를 조회할 때 
	B 엔티티는 로딩되지 않고, 실제로 B 엔티티가 필요한 시점에 로딩됩니다. 
	반면에, fetchType.EAGER 옵션을 사용하면 A 엔티티를 조회할 때 B 엔티티도 함께 로딩됩니다.
	STS 게시판에서 fetchType.LAZY를 사용하면, 해당 게시글과 관련된 다른 엔티티(예: 작성자, 댓글 등)는 
	게시글을 조회하는 시점에 로딩되지 않고, 실제로 필요한 시점에 로딩됩니다.
	 */
}
