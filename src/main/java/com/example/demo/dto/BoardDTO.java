package com.example.demo.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class BoardDTO {
	
	private int bno;
	private String title;
	private String content;
	private String writer;
	private int hits;
	private LocalDateTime regdate;
	private LocalDateTime moddate;

}
